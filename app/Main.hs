module Main where

import Control.Monad (forM_, mapM_)
import Data.Fasta.String
import System.Environment (getArgs)

import Lib

main :: IO ()
main = do
  fileNames <- getArgs
  forM_ fileNames $ \fname -> do
    contents <- readFile fname
    let fastaData = parseFasta contents
    let headers = fastaHeader <$> fastaData
    let seqs = fastaSeq <$> fastaData
    let longestFrames = longestFrame <$> seqs
    mapM_ displayResults $ zip headers longestFrames
  
-- | Accepts a header and index describing a reading frame,
-- and then prints it out.
displayResults :: (String, Int) -> IO ()
displayResults (hdr, idx) = do
  putStr $ "Start index for " ++ hdr ++ ": "
  print idx
